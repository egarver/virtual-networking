#!/bin/sh

#  Topo:
#
#  +-----------------------------------------------------------------------+
#  |  RHEL                                                                 |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  |  +-------+  +-------+  +-------+    +-------+  +-------+  +-------+   |
#  |  | netns |  | netns |  | netns |    | netns |  | netns |  | netns |   |
#  |  |vlan1-1|  |vlan1-2|  |vlan1-3|    |vlan3-1|  |vlan3-2|  |vlan3-3|   | 
#  |  +--+----+  +---+---+  +---+---+    +---+---+  +---+---+  +---+---+   |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |     |           |          |            |          |          |       |
#  |  +--+-----------+----------+------------+----------+----------+---+   |
#  |  |                                                                |   |
#  |  |    br0                                                         |   |
#  |  |                                                                |   |
#  |  +---------------------------+------------------------------------+   |
#  |                              |                                        |
#  |                              |                                        |
#  |                              |                                        |
#  |                              |                                        |
#  |  +---------------------------+------------------------------------+   |
#  |  |                                                                |   |
#  |  |    bond0 mode=2 xmit_hash_policy=vlan+srcmac tlb_dynamic_lb=0  |   |
#  |  |                                                                |   |
#  |  +--+------------------------+------------------------------------+   |
#  |     |                        |                                        |
#  |     |                        |                                        |
#  |     |                        |                                        |
#  |     |                        |                                        |
#  |  +--+-----------------+    +-+----------+    +--------------------+   |
#  |  |netns               |    |netns       |    |               netns|   |
#  |  | sw1                +----+ sw2        +----+                sw3 |   |
#  |  +--------------------+    +----------+-+    +-----------------+--+   |
#  |                                       |                        |      |
#  |                                       |                        |      |
#  |                                       |                        |      |
#  |                                       |                        |      |
#  |  +------------------------------------+------------------------+--+   |
#  |  |                                                                |   |
#  |  |    bond1 mode=2 xmit_hash_policy=vlan+srcmac tlb_dynamic_lb=0  |   |
#  |  |                                                                |   |
#  |  +------------------------------------+---------------------------+   |
#  |                                       |                               |
#  |                                       |                               |
#  |                                       |                               |
#  |                                       |                               |
#  |  +------------------------------------+---------------------------+   |
#  |  |                                                                |   |
#  |  |    br4                                                         |   |
#  |  |                                                                |   |
#  |  +--+-----------+----------------------------------+-----------+--+   |
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |     |           |                                  |           |      | 
#  |  +--+----+  +---+---+                          +---+---+  +----+--+   |
#  |  | netns |  | netns |                          | netns |  | netns |   |
#  |  |vlan1-4|  |vlan3-4|                          |vlan1-5|  |vlan3-5|   | 
#  |  +--+----+  +---+---+                          +---+---+  +---+---+   |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  |                                                                       |
#  +-----------------------------------------------------------------------+

#set -x

function cleanup_topo()
{
        echo $-|grep -q e && e_enabled=yes || e_enabled=no
        set +e
	nmcli connection delete br0
	nmcli connection delete br4
	modprobe -rv bonding
	ip netns del vlan1-1
	ip netns del vlan1-2
	ip netns del vlan1-3
	ip netns del vlan1-4
	ip netns del vlan1-5
	ip netns del vlan3-1
	ip netns del vlan3-2
	ip netns del vlan3-3
	ip netns del vlan3-4
	ip netns del vlan3-5
	ip netns del sw1
	ip netns del sw2
	ip netns del sw3
	ip link del vlan1-1-veth0 &>/dev/null
	ip link del vlan1-2-veth0 &>/dev/null
	ip link del vlan1-3-veth0 &>/dev/null
	ip link del vlan1-4-veth0 &>/dev/null
	ip link del vlan1-5-veth0 &>/dev/null
	ip link del vlan3-1-veth0 &>/dev/null
	ip link del vlan3-2-veth0 &>/dev/null
	ip link del vlan3-3-veth0 &>/dev/null
	ip link del vlan3-4-veth0 &>/dev/null
	ip link del vlan3-5-veth0 &>/dev/null
        nmcli conn delete sw1-bond0-veth0 &>/dev/null 
        nmcli conn delete sw2-bond0-veth0 &>/dev/null
        nmcli conn delete bond0 &>/dev/null
        nmcli conn delete sw2-bond1-veth0 &>/dev/null
        nmcli conn delete sw3-bond1-veth0 &>/dev/null 
        nmcli conn delete bond1 &>/dev/null
        ip link del sw1-bond0-veth0 &>/dev/null 
        ip link del sw2-bond0-veth0 &>/dev/null
        ip link del sw2-bond1-veth0 &>/dev/null
        ip link del sw3-bond1-veth0 &>/dev/null
        ip link del sw1-sw2-veth0 &>/dev/null 
        ip link del sw2-sw3-veth0 &>/dev/null
	ip link del vlan1-1-veth1 &>/dev/null
	ip link del vlan1-2-veth1 &>/dev/null
	ip link del vlan1-3-veth1 &>/dev/null
	ip link del vlan1-4-veth1 &>/dev/null
	ip link del vlan1-5-veth1 &>/dev/null
	ip link del vlan3-1-veth1 &>/dev/null
	ip link del vlan3-2-veth1 &>/dev/null
	ip link del vlan3-3-veth1 &>/dev/null
	ip link del vlan3-4-veth1 &>/dev/null
	ip link del vlan3-5-veth1 &>/dev/null
        ip link del sw1-bond0-veth1 &>/dev/null 
        ip link del sw2-bond0-veth1 &>/dev/null
        ip link del sw2-bond1-veth1 &>/dev/null
        ip link del sw3-bond1-veth1 &>/dev/null
        ip link del sw1-sw2-veth1 &>/dev/null 
        ip link del sw2-sw3-veth1 &>/dev/null
	[ "$e_enabled" == "yes" ] && set -e
}

# define mac and ip
ipaddr=252
mac1=$(printf 20:00:00:00:%02x:98 $ipaddr)
mac2=$(printf 22:00:00:00:%02x:99 $ipaddr)
mac3=$(printf 22:22:22:21:%02x:22 $ipaddr)
mac4=$(printf 20:00:00:00:%02x:98 $ipaddr)
mac5=$(printf 26:00:00:00:%02x:99 $ipaddr)
mac6=$(printf 24:22:22:21:%02x:22 $ipaddr)
mac7=$(printf 2a:22:22:21:%02x:20 $ipaddr)
mac8=$(printf 2a:22:22:21:%02x:21 $ipaddr)
mac9=$(printf 2a:22:22:21:%02x:22 $ipaddr)
mac10=$(printf 2a:22:22:21:%02x:23 $ipaddr)
ip1="172.22.$ipaddr.2"
ip2="172.22.$ipaddr.3"
ip3="172.22.$ipaddr.4"
ip4="172.23.$ipaddr.2"
ip5="172.23.$ipaddr.3"
ip6="172.23.$ipaddr.4"
ip7="172.22.$ipaddr.5"
ip8="172.22.$ipaddr.6"
ip9="172.23.$ipaddr.5"
ip10="172.23.$ipaddr.6"

function setup_topo(){
	
	cleanup_topo &>/dev/null || { echo "Warning when cleanup_topo."; }

	# create bridge 
	nmcli connection add con-name br0 type bridge ifname br0 bridge.vlan-filtering yes ipv4.method disable ipv6.method disable bridge.stp no
	nmcli connection up br0
	nmcli connection add con-name br4 type bridge ifname br4 bridge.vlan-filtering yes ipv4.method disable ipv6.method disable bridge.stp no
	nmcli connection up br4
	
	# create netns and veths
	ip netns add vlan1-1
	ip netns add vlan1-2
	ip netns add vlan1-3
	ip netns add vlan1-4
	ip netns add vlan1-5
	ip netns add vlan3-1
	ip netns add vlan3-2
	ip netns add vlan3-3
	ip netns add vlan3-4
	ip netns add vlan3-5
	ip netns add sw1
	ip netns add sw2
	ip netns add sw3
	
	ip link add name vlan1-1-veth0 type veth peer name vlan1-1-veth1
	ip link add name vlan1-2-veth0 type veth peer name vlan1-2-veth1
	ip link add name vlan1-3-veth0 type veth peer name vlan1-3-veth1
	ip link add name vlan1-4-veth0 type veth peer name vlan1-4-veth1
	ip link add name vlan1-5-veth0 type veth peer name vlan1-5-veth1
	ip link add name vlan3-1-veth0 type veth peer name vlan3-1-veth1
	ip link add name vlan3-2-veth0 type veth peer name vlan3-2-veth1
	ip link add name vlan3-3-veth0 type veth peer name vlan3-3-veth1
	ip link add name vlan3-4-veth0 type veth peer name vlan3-4-veth1
	ip link add name vlan3-5-veth0 type veth peer name vlan3-5-veth1
	ip link add name sw1-bond0-veth0 type veth peer name sw1-bond0-veth1
	ip link add name sw2-bond0-veth0 type veth peer name sw2-bond0-veth1
	ip link add name sw2-bond1-veth0 type veth peer name sw2-bond1-veth1
	ip link add name sw3-bond1-veth0 type veth peer name sw3-bond1-veth1
	
	# connect netns vlan1-1 and br0
	ip link set vlan1-1-veth0 netns vlan1-1 up
	ip link set vlan1-1-veth1 master br0 up
	
	# connect netns vlan1-2 and br0
	ip link set vlan1-2-veth0 netns vlan1-2 up
	ip link set vlan1-2-veth1 master br0 up
	
	# connect netns vlan1-3 and br0
	ip link set vlan1-3-veth0 netns vlan1-3 up
	ip link set vlan1-3-veth1 master br0 up
	
	# connect netns vlan1-4 and br4
	ip link set vlan1-4-veth0 master br4 up
	ip link set vlan1-4-veth1 netns vlan1-4 up
	
	# connect netns vlan1-5 and br4
	ip link set vlan1-5-veth0 master br4 up
	ip link set vlan1-5-veth1 netns vlan1-5 up
	
	# connect netns vlan3-1 and br0
	ip link set vlan3-1-veth0 netns vlan3-1 up
	ip link set vlan3-1-veth1 master br0 up
	
	# connect netns vlan3-2 and br0
	ip link set vlan3-2-veth0 netns vlan3-2 up
	ip link set vlan3-2-veth1 master br0 up
	
	# connect netns vlan3-3 and br0
	ip link set vlan3-3-veth0 netns vlan3-3 up
	ip link set vlan3-3-veth1 master br0 up
	
	# connect netns vlan3-4 and br4
	ip link set vlan3-4-veth0 master br4 up
	ip link set vlan3-4-veth1 netns vlan3-4 up
	
	# connect netns vlan3-5 and br4
	ip link set vlan3-5-veth0 master br4 up
	ip link set vlan3-5-veth1 netns vlan3-5 up
	
	ip link set sw1-bond0-veth1 netns sw1 up
	ip link set sw2-bond0-veth1 netns sw2 up
	ip link set sw2-bond1-veth1 netns sw2 up
	ip link set sw3-bond1-veth1 netns sw3 up
	
	# copy dispatcher script
	NM_DISPATCHERD="/etc/NetworkManager/dispatcher.d/"
	cp $LIB_DIR/mlag.sh "${NM_DISPATCHERD}"
	chmod +x "${NM_DISPATCHERD}/mlag.sh"
	trap "rm ${NM_DISPATCHERD}/mlag.sh" EXIT

	# bond-slb config
	echo 'BOND_SLB_IFACES="bond0 bond1"' >  /etc/default/bond-slb
	trap "rm /etc/default/bond-slb" EXIT

	# create bond with nmcli
	#
	nmcli connection add con-name bond0 type bond ifname bond0 master br0 bond.options "mode=2,xmit_hash_policy=vlan+srcmac"
	nmcli connection add con-name sw1-bond0-veth0 type ethernet ifname sw1-bond0-veth0 master bond0
	nmcli connection add con-name sw2-bond0-veth0 type ethernet ifname sw2-bond0-veth0 master bond0
	#
	nmcli connection add con-name bond1 type bond ifname bond1 master br4 bond.options "mode=2,xmit_hash_policy=vlan+srcmac"
	nmcli connection add con-name sw2-bond1-veth0 type ethernet ifname sw2-bond1-veth0 master bond1
	nmcli connection add con-name sw3-bond1-veth0 type ethernet ifname sw3-bond1-veth0 master bond1
	#
	nmcli connection up sw1-bond0-veth0
	nmcli connection up sw2-bond0-veth0
	nmcli connection up sw2-bond1-veth0
	nmcli connection up sw3-bond1-veth0
	nmcli connection up bond0
	nmcli connection up bond1

	sleep 10
	
	# create bridge in sw1 and connect bond0 to sw1 
	ip netns exec sw1 ip link add name br1 type bridge vlan_filtering 1
	ip netns exec sw1 ip link set br1 up
	ip netns exec sw1 ip link set sw1-bond0-veth1 master br1
	
	# create bridge in sw3 and connect bond1 to sw3
	ip netns exec sw3 ip link add name br3 type bridge vlan_filtering 1
	ip netns exec sw3 ip link set br3 up
	ip netns exec sw3 ip link set sw3-bond1-veth1 master br3

	# create bridge in sw2 and connect bond0,bond1 to sw2
	ip netns exec sw2 ip link add name br2 type bridge vlan_filtering 1
	ip netns exec sw2 ip link set br2 up
	ip netns exec sw2 ip link set sw2-bond0-veth1 master br2
	ip netns exec sw2 ip link set sw2-bond1-veth1 master br2

	# connect sw1 sw2 and sw3
	ip link add name sw1-sw2-veth0 type veth peer name sw1-sw2-veth1	
	ip link add name sw2-sw3-veth0 type veth peer name sw2-sw3-veth1	
	ip link set sw1-sw2-veth0 netns sw1 up
	ip link set sw1-sw2-veth1 netns sw2 up
	ip netns exec sw1 ip link set sw1-sw2-veth0 master br1
	ip netns exec sw2 ip link set sw1-sw2-veth1 master br2
	ip link set sw2-sw3-veth0 netns sw2 up
	ip link set sw2-sw3-veth1 netns sw3 up
	ip netns exec sw2 ip link set sw2-sw3-veth0 master br2
	ip netns exec sw3 ip link set sw2-sw3-veth1 master br3


	# setup ip for netns
	ip netns exec vlan1-1 ip link set vlan1-1-veth0 up
	ip netns exec vlan1-1 ip addr add $ip1/24 dev vlan1-1-veth0 
	ip netns exec vlan1-2 ip link set vlan1-2-veth0 up
	ip netns exec vlan1-2 ip addr add $ip2/24 dev vlan1-2-veth0 
	ip netns exec vlan1-3 ip link set vlan1-3-veth0 up
	ip netns exec vlan1-3 ip addr add $ip3/24 dev vlan1-3-veth0 
	ip netns exec vlan3-1 ip link set vlan3-1-veth0 up
	ip netns exec vlan3-1 ip addr add $ip4/24 dev vlan3-1-veth0 
	ip netns exec vlan3-2 ip link set vlan3-2-veth0 up
	ip netns exec vlan3-2 ip addr add $ip5/24 dev vlan3-2-veth0 
	ip netns exec vlan3-3 ip link set vlan3-3-veth0 up
	ip netns exec vlan3-3 ip addr add $ip6/24 dev vlan3-3-veth0 
	
	ip netns exec vlan1-4 ip link set vlan1-4-veth1 up
	ip netns exec vlan1-4 ip addr add $ip7/24 dev vlan1-4-veth1
	ip netns exec vlan1-5 ip link set vlan1-5-veth1 up
	ip netns exec vlan1-5 ip addr add $ip8/24 dev vlan1-5-veth1
	ip netns exec vlan3-4 ip link set vlan3-4-veth1 up
	ip netns exec vlan3-4 ip addr add $ip9/24 dev vlan3-4-veth1
	ip netns exec vlan3-5 ip link set vlan3-5-veth1 up
	ip netns exec vlan3-5 ip addr add $ip10/24 dev vlan3-5-veth1
	
	# change veth mac
	ip netns exec vlan1-1 ip link set vlan1-1-veth0 address $mac1 
	ip netns exec vlan1-2 ip link set vlan1-2-veth0 address $mac2
	ip netns exec vlan1-3 ip link set vlan1-3-veth0 address $mac3
	ip netns exec vlan3-1 ip link set vlan3-1-veth0 address $mac4
	ip netns exec vlan3-2 ip link set vlan3-2-veth0 address $mac5
	ip netns exec vlan3-3 ip link set vlan3-3-veth0 address $mac6
	ip netns exec vlan1-4 ip link set vlan1-4-veth1 address $mac7
	ip netns exec vlan1-5 ip link set vlan1-5-veth1 address $mac8
	ip netns exec vlan3-4 ip link set vlan3-4-veth1 address $mac9
	ip netns exec vlan3-5 ip link set vlan3-5-veth1 address $mac10
	
	
	# setup vlan
	bridge vlan add vid 3 dev bond0
	bridge vlan add vid 3 dev bond1
	bridge vlan del vid 1 dev vlan3-1-veth1
	bridge vlan add vid 3 dev vlan3-1-veth1 pvid untagged
	bridge vlan del vid 1 dev vlan3-2-veth1
	bridge vlan add vid 3 dev vlan3-2-veth1 pvid untagged
	bridge vlan del vid 1 dev vlan3-3-veth1
	bridge vlan add vid 3 dev vlan3-3-veth1 pvid untagged
	
	ip netns exec sw1 bridge vlan add vid 3 dev sw1-bond0-veth1 
	ip netns exec sw1 bridge vlan add vid 3 dev sw1-sw2-veth0 
	ip netns exec sw2 bridge vlan add vid 3 dev sw2-bond0-veth1 
	ip netns exec sw2 bridge vlan add vid 3 dev sw2-bond1-veth1 
	ip netns exec sw2 bridge vlan add vid 3 dev sw1-sw2-veth1
	ip netns exec sw2 bridge vlan add vid 3 dev sw2-sw3-veth0
	ip netns exec sw3 bridge vlan add vid 3 dev sw2-sw3-veth1
	ip netns exec sw3 bridge vlan add vid 3 dev sw3-bond1-veth1
	
	bridge vlan del vid 1 dev vlan3-4-veth0
	bridge vlan add vid 3 dev vlan3-4-veth0 pvid untagged
	bridge vlan del vid 1 dev vlan3-5-veth0
	bridge vlan add vid 3 dev vlan3-5-veth0 pvid untagged

}

setup_topo
