#!/usr/bin/env sh
#
# This is meant to be used as a NetworkManager-dispatcher script.
#
# This script implements part one of a software implementation for multi
# chassis link aggregation (MLAG); also similar to Open vSwitch's bonding-slb.
#
# The second part of the implementation is the Linux bonding driver where bonds
# have mode=balance-xor and xmit_hash_policy=vlan+srcmac. This dispatch script
# functions only on these bonds and will ignore all other interfaces.
#
#
# Usage:
#
# Copy dispatcher script:
#
#   cp mlag.sh /etc/NetworkManager/dispatcher.d/
#   chmod +x /etc/NetworkManager/dispatcher.d/mlag.sh
#
# Create a bond:
#
#   nmcli connection add con-name bond0 type bond ifname bond0 bond.options "mode=2,xmit_hash_policy=vlan+srcmac"
#   nmcli connection add con-name bond0-member1 type ethernet ifname <interface> master bond0
#   nmcli connection add con-name bond0-member2 type ethernet ifname <interface> master bond0
#   nmcli connection up bond0-member1
#   nmcli connection up bond0-member2
#   nmcli connection up bond0
#
# Update bond-slb config:
#
#   echo 'BOND_SLB_IFACES="bond0"' >> /etc/default/bond-slb
#
# args: <interface> <nm_action>
set -e

parse_args() {
	NM_INTERFACE="$1"
	NM_ACTION="$2"
}

sanity_check() {
	# jq is needed to parse ip-link data
	command -v jq >/dev/null || { echo "$(basename ${0}): ERROR: jq must be installed!" >&2; exit 0; }

	test "$NM_ACTION" = "up" || test "$NM_ACTION" = "down" || exit 0
}

get_bond_facts() {
	# make sure it's a bond member
	NM_INTERFACE_KIND="$(ip -d -j link show ${NM_INTERFACE} | jq --raw-output .[0].linkinfo.info_slave_kind 2> /dev/null)"
	test "${NM_INTERFACE_KIND}" = "bond" || exit 0

	BOND="$(ip -j link show ${NM_INTERFACE} | jq --raw-output .[0].master 2> /dev/null)"
	test -n "${BOND}" || exit 0

	# "elect" a primary bond member by sorting the result
	BOND_ACTIVE_MEMBERS="$(ip -j link show master ${BOND} | jq --raw-output 'map(select(length > 0)) | map(select(.operstate == "UP")) | sort_by(.ifname) | .[].ifname' 2> /dev/null | tr '\n' ' ')"
}

sanity_check_bond() {
	# verify the bond is using vlan+srcmac hashing
	test "balance-xor" = "$(ip -d -j link show ${BOND} | jq --raw-output .[0].linkinfo.info_data.mode 2> /dev/null)" || exit 0
	test "vlan+srcmac" = "$(ip -d -j link show ${BOND} | jq --raw-output .[0].linkinfo.info_data.xmit_hash_policy 2> /dev/null)" || exit 0

	# verify configuration file says this is a desired bond slb
	BOND_SLB_CONF="/etc/default/bond-slb"
	grep -e "^BOND_SLB_IFACES=.*\b${BOND}\b.*" "${BOND_SLB_CONF}" >/dev/null || exit 0
}

nft_do_init() {
	nft list table netdev mlag-${BOND} >/dev/null 2>&1 && \
	echo "delete table netdev mlag-${BOND}"

	if test -n "${BOND_ACTIVE_MEMBERS}"; then
		echo "add table netdev mlag-${BOND}"
	fi
}

# OVS SLB rule 1
#
# "Open vSwitch avoids packet duplication by accepting multicast and broadcast
# packets on only the active member, and dropping multicast and broadcast
# packets on all other members."
#
nft_do_bond_slb_rule1() {
	# primary is first member, we drop on all others
	for I in ${BOND_ACTIVE_MEMBERS#* }; do
		echo "add chain netdev mlag-${BOND} rx-drop-bc-mc-${I} { type filter hook ingress device ${I} priority filter; }"
		echo "add rule  netdev mlag-${BOND} rx-drop-bc-mc-${I} pkttype { broadcast, multicast } counter drop"
	done
}

# OVS SLB rule 2
#
# "Open vSwitch deals with this case by dropping packets received on any SLB
# bonded link that have a source MAC+VLAN that has been learned on any other
# port."
#
nft_do_bond_slb_rule2() {
	cat <<-HERE
		add set netdev mlag-${BOND} macset-tagged { typeof ether saddr . vlan id; flags timeout; }
		add set netdev mlag-${BOND} macset-untagged { typeof ether saddr; flags timeout; }

		add chain netdev mlag-${BOND} tx-snoop-source-mac { type filter hook egress device ${BOND} priority filter; }
		add rule netdev mlag-${BOND} tx-snoop-source-mac set update ether saddr . vlan id timeout 5s @macset-tagged counter return # tagged
		add rule netdev mlag-${BOND} tx-snoop-source-mac set update ether saddr timeout 5s @macset-untagged counter # untagged

		add chain netdev mlag-${BOND} rx-drop-looped-packets { type filter hook ingress device ${BOND} priority filter; }
		add rule netdev mlag-${BOND} rx-drop-looped-packets ether saddr . vlan id @macset-tagged counter drop
		add rule netdev mlag-${BOND} rx-drop-looped-packets ether type vlan counter return # avoid looking up tagged packets in untagged table
		add rule netdev mlag-${BOND} rx-drop-looped-packets ether saddr @macset-untagged counter drop
	HERE
}

# This redirects all IGMP reports to the primary member port. The TOR switches
# may prune the multicast tree. If we let the bonding vlan+srcmac hash occur,
# then the TOR may send the pruned multicast stream to a bond member port for
# which the RX filters will drop (e.g. report out non-primary, stream in
# non-primary). If it's known multicast then we must control the pruned tree by
# only sending the IGMP reports on a port for which we will accept the traffic,
# i.e. the primary member port.
#
nft_do_igmp_snooping() {
	# redirect all IGMP reports to the primary port
	for I in ${BOND_ACTIVE_MEMBERS#* }; do
		echo "add chain netdev mlag-${BOND} tx-redirect-igmp-reports-${I} { type filter hook egress device ${I} priority filter + 1; }"
		echo "add rule  netdev mlag-${BOND} tx-redirect-igmp-reports-${I} igmp type { membership-report-v1, membership-report-v2, membership-report-v3 } counter fwd to ${BOND_ACTIVE_MEMBERS%% *}"
		echo "add rule  netdev mlag-${BOND} tx-redirect-igmp-reports-${I} icmpv6 type { mld-listener-report, mld2-listener-report } counter fwd to ${BOND_ACTIVE_MEMBERS%% *}"
	done
}

nft_do_mlag() {
	NFT_RULES=$(mktemp)
	{
	nft_do_init
	if test -n "${BOND_ACTIVE_MEMBERS}"; then
		nft_do_bond_slb_rule1
		nft_do_bond_slb_rule2
		#nft_do_igmp_snooping
	fi
	} > "${NFT_RULES}"
	nft -f "${NFT_RULES}"
	rm "${NFT_RULES}"
}

parse_args "$@"
sanity_check
get_bond_facts
sanity_check_bond
nft_do_mlag
echo "$(basename ${0}) configured ${BOND} with active members: ${BOND_ACTIVE_MEMBERS}."
