#!/bin/sh
# Virtual network topology for leaf-spine infrastructure
#
#                  +------+                                           +------+
#   host1 --link1--|      |         +--link3-- sw2 --link5--+         |      |---link7---host3
#                  | sw1  |--bond1--+                       +--bond2--|  sw4 |
#   host2 --link2--|      |         +--link4-- sw3 --link6--+         |      |---link8---host4
#                  +------+                                           +------+
#
# Layer 3 (IP) topology:
#
#   10.0.0.0/24 with host octect being 1x where x == host number, thus
#
# host1 - 10.0.0.11/24
# host2 - 10.0.0.12/24
# host3 - 10.0.0.13/24
# host4 - 10.0.0.14/24
#

BOND_MODE=${BOND_MODE:-6}
BOND_OPTS=${BOND_SET:-xmit_hash_policy vlan+srcmac tlb_dynamic_lb 0}

# reset: delete all net namespaces and start again
for n in sw1 sw2 sw3 host1 host2 host3 host4; do
	ip netns delete ${n}
done

echo 1 >/proc/sys/net/ipv4/ip_forward

# create all 'wires' using veth
for i in $(seq 1 1 8); do
	ip link add dev link${i}_1 type veth peer name link${i}_2
done

# create all 'nodes' using net name spaces
for n in host1 host2 host3 host4 sw1 sw2 sw3 sw4; do
	ip netns add ${n}
done

# assign wire ends to nodes
ip link set dev link1_1 netns host1 up
ip link set dev link1_2 netns sw1   up
ip link set dev link2_1 netns host2 up
ip link set dev link2_2 netns sw1   up
ip link set dev link3_1 netns sw1   up
ip link set dev link3_2 netns sw2   up
ip link set dev link4_1 netns sw1   up
ip link set dev link4_2 netns sw3   up
ip link set dev link5_1 netns sw2   up
ip link set dev link5_2 netns sw4   up
ip link set dev link6_1 netns sw3   up
ip link set dev link6_2 netns sw4   up
ip link set dev link7_1 netns sw4   up
ip link set dev link7_2 netns host3 up
ip link set dev link8_1 netns sw4   up
ip link set dev link8_2 netns host4 up

# assign IP addresses to hosts
ip netns exec host1 ip addr add 10.0.0.11/24 dev link1_1
ip netns exec host2 ip addr add 10.0.0.12/24 dev link2_1
ip netns exec host3 ip addr add 10.0.0.13/24 dev link7_2
ip netns exec host4 ip addr add 10.0.0.14/24 dev link8_2

# setup sw1
ip netns exec sw1 ip link add dev br1 up type bridge
#ip netns exec sw1 brctl stp br1 on
ip netns exec sw1 ip link add dev bond1 up type bond mode ${BOND_MODE} ${BOND_OPTS}
ip netns exec sw1 ip link set dev link3_1 down master bond1 # NOTE: add link3_1 first to make it active slave.
ip netns exec sw1 ip link set dev link4_1 down master bond1
ip netns exec sw1 ip link set dev bond1 master br1
ip netns exec sw1 ip link set dev link1_2 master br1
ip netns exec sw1 ip link set dev link2_2 master br1

# setup sw2
ip netns exec sw2 ip link add dev br2 up type bridge
#ip netns exec sw2 brctl stp br2 on
ip netns exec sw2 ip link set dev link3_2 master br2
ip netns exec sw2 ip link set dev link5_1 master br2

# setup sw3
ip netns exec sw3 ip link add dev br3 up type bridge
#ip netns exec sw3 brctl stp br3 on
ip netns exec sw3 ip link set dev link4_2 master br3
ip netns exec sw3 ip link set dev link6_1 master br3

# setup sw4
ip netns exec sw4 ip link add dev br4 up type bridge
ip netns exec sw4 ip link add dev bond2 up type bond mode ${BOND_MODE} ${BOND_OPTS}
ip netns exec sw4 ip link set dev link6_2 down master bond2 # NOTE: add link6_2 first to make it active slave.
ip netns exec sw4 ip link set dev link5_2 down master bond2
ip netns exec sw4 ip link set dev bond2 master br4
#ip netns exec sw4 brctl stp br4 on
ip netns exec sw4 ip link set dev link7_1 master br4
ip netns exec sw4 ip link set dev link8_1 master br4

echo "virtual network leaf-spine setup complete"
